#!/bin/bash
conda install jupyter -y --quiet
#source /opt/conda/etc/profile.d/conda.sh
#conda env create -f /opt/notebooks/environment.yaml
#conda activate data_literacy

jupyter notebook --notebook-dir=/opt/notebooks --ip=0.0.0.0 --port=8889 --no-browser --allow-root
#--NotebookApp.password='{{ jupyter_server_password }}'
